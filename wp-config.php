<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'F/0AGvOcPgXWx9BxilNUOtuGLJF5vCp/pDnBRhTvvxUQIykOgpARmJF2A7MFVbLIPjt53ip4qPjoDXt7icdh0Q==');
define('SECURE_AUTH_KEY',  'iHcU/TmZ+rzVFk7xJ/3kCI6LDJxTIdtZFowB+nfM6Se26VBaj40LqYNIlVu3JhghuwB08P9uOj31f7Mf2Ogp/A==');
define('LOGGED_IN_KEY',    'RFpwHSlD24f9C84p/3ErjP6sHeuj+I5p9Mf3KZgKdGSYwsAGBpM45UrvJh+keczROgqNQ0DAcQe8MEb3xJK+Ng==');
define('NONCE_KEY',        '+oq0hY6KFQnXeAsaf2B4QeGyKxSsHEHtTUdQd20Gnr0NA73rYhI4ZFuXHYJwbTQ5NesW4Z3ZIt8fmuImtgZZXw==');
define('AUTH_SALT',        'o5GlNoKfC3tjx9K/5txPhfhPZpEWkA14Kw38m5iYN0JWf5vLKq/hNstUXWr/frgsppIiaoUb/Jx5xNayjG8nAQ==');
define('SECURE_AUTH_SALT', 'CJIDuiaMJkHo+aiwbtXisyEtynhRk2JO3IKb43eXmw+3RoDtDdMbDB6NGQrp9vn/y9xwV+Df8QiKRQgeaB4Hmg==');
define('LOGGED_IN_SALT',   'JV7B6UNJJ5QYYB4/cym/3Pc/gMge5/9M1+JaaeLwQ8sfD4P+7faY/9Slw9hwoDnZyYPhls/86EiYsKkYZyRNIQ==');
define('NONCE_SALT',       'PwqwxBii0gUHURg2IDiy7ut5/y0lBSRYZ86C/tjCy3TP8A/YdcEK59EiXBfgDC5XSCu5sZAKzarXJed1nR5DWQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
