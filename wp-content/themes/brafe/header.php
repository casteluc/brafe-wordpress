<!DOCTYPE html>
<html lang="pt-bt">
<head>
    <title>Brafé</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head(); ?>
</head>
<body>
    
    <header>
        <a href="/#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="brafé"></a>

        <nav>
            <?php 
                $args = array(
                    'menu' => 'principal',
                    'theme_location' => 'navegacao',
                    'container' => 'false'
                );

                wp_nav_menu( $args );
            ?>
        </nav>
    </header>